namespace MVCGettingStarted.Migrations
{
    using MVCGettingStarted.Models;
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<MyRecipesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MyRecipesContext context)
        {
            context.MeasurementUnits.AddOrUpdate(mu => mu.Name, 
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "gram", Shorthand = "g" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "kilogram", Shorthand = "kg" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "cup", Shorthand = "c" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "pound", Shorthand = "lb" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "ounce", Shorthand = "oz" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "fluid ounce", Shorthand = "fl oz" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "millilitre", Shorthand = "ml" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "litre", Shorthand = "l" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "tablespoon", Shorthand = "tbsp" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "teaspoon", Shorthand = "tsp" },
                new MeasurementUnit() { Uid = Guid.NewGuid(), Name = "piece", Shorthand = "pc" }
            );
        }
    }
}
