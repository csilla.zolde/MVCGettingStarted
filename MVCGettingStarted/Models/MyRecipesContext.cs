﻿using System.Data.Entity;

namespace MVCGettingStarted.Models
{
    public class MyRecipesContext : DbContext
    {
        public MyRecipesContext() : base("MyRecipesDatabase") { }

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<MeasurementUnit> MeasurementUnits { get; set; }
    }
}