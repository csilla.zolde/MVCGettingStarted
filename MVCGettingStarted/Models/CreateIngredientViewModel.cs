﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVCGettingStarted.Models
{
    public class CreateIngredientViewModel
    {
        [Key]
        public Guid Uid { get; set; }

        public string Name { get; set; }

        public Guid MeasurementUnitUid { get; set; }
        public List<MeasurementUnit> MeasurementUnits { get; set; }

        public double? Price { get; set; }
    }
}