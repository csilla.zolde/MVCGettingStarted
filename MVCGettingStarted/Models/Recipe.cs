﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVCGettingStarted.Models
{
    public class Recipe : RecipeObject
    {
        public virtual List<RecipeIngredient> Ingredients { get; set; }

        [Required]
        public virtual List<Step> Steps { get; set; }
    }
}