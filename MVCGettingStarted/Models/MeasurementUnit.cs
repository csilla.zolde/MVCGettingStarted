﻿namespace MVCGettingStarted.Models
{
    public class MeasurementUnit : RecipeObject
    {
        public string Shorthand { get; set; }
    }
}