﻿using System;

namespace MVCGettingStarted.Models
{
    public class Ingredient : RecipeObject
    {
        public Guid MeasurementUnitUid { get; set; }
        public virtual MeasurementUnit MeasurementUnit { get; set; }

        public double? Price { get; set; }
    }
}